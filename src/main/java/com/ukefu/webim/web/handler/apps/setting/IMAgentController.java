package com.ukefu.webim.web.handler.apps.setting;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.util.Menu;
import com.ukefu.util.UKTools;
import com.ukefu.webim.service.repository.BlackListRepository;
import com.ukefu.webim.service.repository.ConsultInviteRepository;
import com.ukefu.webim.service.repository.QuickReplyRepository;
import com.ukefu.webim.service.repository.SessionConfigRepository;
import com.ukefu.webim.service.repository.TagRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.QuickReply;
import com.ukefu.webim.web.model.SessionConfig;
import com.ukefu.webim.web.model.Tag;

@Controller
@RequestMapping("/setting")
public class IMAgentController extends Handler{
	
	@Autowired
	private ConsultInviteRepository invite;
	
	@Autowired
	private SessionConfigRepository sessionConfigRes ;
	
	@Autowired
	private TagRepository tagRes ;
	
	@Autowired
	private QuickReplyRepository quickReplyRes ;
	
	@Autowired
	private BlackListRepository blackListRes;
	
	@Value("${web.upload-path}")
    private String path;

    @RequestMapping("/agent/index")
    @Menu(type = "setting" , subtype = "sessionconfig" , admin= true)
    public ModelAndView index(ModelMap map , HttpServletRequest request) {
    	SessionConfig sessionConfig = sessionConfigRes.findByOrgi(super.getOrgi(request)) ;
    	if(sessionConfig == null){
    		sessionConfig = new SessionConfig() ;
    	}
    	map.put("sessionConfig", sessionConfig) ;
        return request(super.createAppsTempletResponse("/apps/setting/agent/index"));
    }
    
    @RequestMapping("/agent/sessionconfig/save")
    @Menu(type = "setting" , subtype = "sessionconfig" , admin= true)
    public ModelAndView sessionconfig(ModelMap map , HttpServletRequest request , @Valid SessionConfig sessionConfig) {
    	SessionConfig tempSessionConfig = sessionConfigRes.findByOrgi(super.getOrgi(request)) ;
    	if(tempSessionConfig == null){
    		tempSessionConfig = sessionConfig;
    		tempSessionConfig.setCreater(super.getUser(request).getId());
    	}else{
    		UKTools.copyProperties(sessionConfig, tempSessionConfig);
    	}
    	tempSessionConfig.setOrgi(super.getOrgi(request));
    	sessionConfigRes.save(tempSessionConfig) ;
    	map.put("sessionConfig", tempSessionConfig) ;
        return request(super.createAppsTempletResponse("/apps/setting/agent/index"));
    }
    
    @RequestMapping("/blacklist")
    @Menu(type = "setting" , subtype = "blacklist" , admin= true)
    public ModelAndView blacklist(ModelMap map , HttpServletRequest request) {
    	map.put("blackList", blackListRes.findByOrgi(super.getOrgi(request), new PageRequest(super.getP(request), super.getPs(request)))) ;
        return request(super.createAppsTempletResponse("/apps/setting/agent/blacklist"));
    }
    
    @RequestMapping("/blacklist/delete")
    @Menu(type = "setting" , subtype = "tag" , admin= true)
    public ModelAndView blacklistdelete(ModelMap map , HttpServletRequest request , @Valid String id) {
    	blackListRes.delete(id);
    	return request(super.createRequestPageTempletResponse("redirect:/setting/blacklist.html"));
    }
    
    @RequestMapping("/tag")
    @Menu(type = "setting" , subtype = "tag" , admin= true)
    public ModelAndView tag(ModelMap map , HttpServletRequest request) {
    	map.put("tagList", tagRes.findByOrgi(super.getOrgi(request) , new PageRequest(super.getP(request), super.getPs(request)))) ;
        return request(super.createAppsTempletResponse("/apps/setting/agent/tag"));
    }
    
    @RequestMapping("/tag/add")
    @Menu(type = "setting" , subtype = "tag" , admin= true)
    public ModelAndView tagadd(ModelMap map , HttpServletRequest request) {
        return request(super.createAppsTempletResponse("/apps/setting/agent/tagadd"));
    }
    
    @RequestMapping("/tag/edit")
    @Menu(type = "setting" , subtype = "tag" , admin= true)
    public ModelAndView tagedit(ModelMap map , HttpServletRequest request , @Valid String id) {
    	map.put("tag", tagRes.findOne(id)) ;
        return request(super.createAppsTempletResponse("/apps/setting/agent/tagedit"));
    }
    
    @RequestMapping("/tag/update")
    @Menu(type = "setting" , subtype = "tag" , admin= true)
    public ModelAndView tagupdate(ModelMap map , HttpServletRequest request , @Valid Tag tag) {
    	Tag temptag = tagRes.findByOrgiAndTag(super.getOrgi(request), tag.getTag()) ;
    	if(temptag == null || tag.getId().equals(temptag.getId())){
    		tag.setOrgi(super.getOrgi(request));
	    	tag.setCreater(super.getUser(request).getId());
	    	tagRes.save(tag) ;
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/setting/tag.html"));
    }
    
    @RequestMapping("/tag/save")
    @Menu(type = "setting" , subtype = "tag" , admin= true)
    public ModelAndView tagsave(ModelMap map , HttpServletRequest request , @Valid Tag tag) {
    	if(tagRes.findByOrgiAndTag(super.getOrgi(request), tag.getTag()) == null){
	    	tag.setOrgi(super.getOrgi(request));
	    	tag.setCreater(super.getUser(request).getId());
	    	tagRes.save(tag) ;
    	}
        return request(super.createRequestPageTempletResponse("redirect:/setting/tag.html"));
    }
    
    @RequestMapping("/tag/delete")
    @Menu(type = "setting" , subtype = "tag" , admin= true)
    public ModelAndView tagdelete(ModelMap map , HttpServletRequest request , @Valid String id) {
    	tagRes.delete(id);
    	return request(super.createRequestPageTempletResponse("redirect:/setting/tag.html"));
    }
    
    
    
    @RequestMapping("/acd")
    @Menu(type = "setting" , subtype = "acd" , admin= true)
    public ModelAndView acd(ModelMap map , HttpServletRequest request) {
        return request(super.createAppsTempletResponse("/apps/setting/agent/acd"));
    }
    
    @RequestMapping("/quickreply")
    @Menu(type = "setting" , subtype = "quickreply" , admin= true)
    public ModelAndView kuaijie(ModelMap map , HttpServletRequest request) {
    	map.put("quickReplyList", quickReplyRes.findByOrgi(super.getOrgi(request))) ;
        return request(super.createAppsTempletResponse("/apps/setting/agent/quickreply"));
    }
    @RequestMapping("/quickreply/add")
    @Menu(type = "setting" , subtype = "quickreplyadd" , admin= true)
    public ModelAndView quickreplyadd(ModelMap map , HttpServletRequest request) {
        return request(super.createAppsTempletResponse("/apps/setting/agent/quickreplyadd"));
    }
    
    @RequestMapping("/quickreply/save")
    @Menu(type = "setting" , subtype = "quickreply" , admin= true)
    public ModelAndView quickreplysave(ModelMap map , HttpServletRequest request , @Valid QuickReply quickReply) {
    	if(!StringUtils.isBlank(quickReply.getTitle()) && !StringUtils.isBlank(quickReply.getContent())){
	    	quickReply.setOrgi(super.getOrgi(request));
			quickReply.setCreater(super.getUser(request).getId());
			quickReplyRes.save(quickReply) ;
    	}
        return request(super.createRequestPageTempletResponse("redirect:/setting/quickreply.html"));
    }
    
    @RequestMapping("/quickreply/delete")
    @Menu(type = "setting" , subtype = "quickreply" , admin= true)
    public ModelAndView quickreplydelete(ModelMap map , HttpServletRequest request , @Valid String id) {
    	quickReplyRes.delete(id);
    	return request(super.createRequestPageTempletResponse("redirect:/setting/quickreply.html"));
    }
    @RequestMapping("/quickreply/edit")
    @Menu(type = "setting" , subtype = "quickreply" , admin= true)
    public ModelAndView quickreplyedit(ModelMap map , HttpServletRequest request , @Valid String id) {
    	map.put("quickReply", quickReplyRes.findOne(id)) ;
        return request(super.createAppsTempletResponse("/apps/setting/agent/quickreplyedit"));
    }
    
    @RequestMapping("/quickreply/update")
    @Menu(type = "setting" , subtype = "quickreply" , admin= true)
    public ModelAndView quickreplyupdate(ModelMap map , HttpServletRequest request , @Valid QuickReply quickReply) {
    	if(!StringUtils.isBlank(quickReply.getId())){
    		quickReply.setOrgi(super.getOrgi(request));
    		quickReply.setCreater(super.getUser(request).getId());
    		quickReplyRes.save(quickReply) ;
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/setting/quickreply.html"));
    }
    
}