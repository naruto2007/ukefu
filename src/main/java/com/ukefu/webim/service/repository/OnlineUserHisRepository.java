package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.OnlineUserHis;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface OnlineUserHisRepository
  extends JpaRepository<OnlineUserHis, String>
{
  public abstract OnlineUserHis findById(String paramString);
  
  public abstract OnlineUserHis findBySessionid(String paramString);
}
